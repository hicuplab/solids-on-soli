#!/usr/bin/env python3
# coding: utf-8

'''
Preprocess Soli files (frame averaging and padding/truncation).

External dependencies, to be installed e.g. via pip:
- tensorflow v1.13
- numpy v1.16
- Pillow v6.0

Author: Luis A. Leiva <luis.leiva@aalto.fi>
'''

import sys
import os
import argparse

# Define CLI options.
parser = argparse.ArgumentParser(description='Preprocess Soli files.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--dataset_dir', required=True, help='path to recorded soli data directory')
#parser.add_argument('--dataset_files', nargs='+', help='path to individual soli files. DEBUG ONLY')

args = parser.parse_args()
print(args, file=sys.stderr)

from utils import prepare_data_dir

DATASET_AVG = os.path.normpath(args.dataset_dir) + '_averaged'
prepare_data_dir(args.dataset_dir, DATASET_AVG)

print('Done. You can delete the files in {}, but leave the directory empty.'.format(args.dataset_dir))
