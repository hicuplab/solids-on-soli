# Solids on soli

This repository hosts data relevant to our publication: Solids on Soli: Millimetre-Wave Radar Sensing through Materials. If you use any of the published content please reference our papers.

Klen Čopič Pucihar, Nuwan T. Attygalle, Matjaž Kljun, Christian Sandor, and Luis A. Leiva. 2022. Solids on
Soli: Millimetre-Wave Radar Sensing through Materials. Proc. ACM Hum.-Comput. Interact. , (2022), 20 pages.
https://doi.org/10.1145/3532212

Nuwan T. Attygalle, Luis A. Leiva, Matjaž Kljun, Christian Sandor, Alexander Plopski, Hirokazu Kato, and Klen Čopič Pucihar. 2021. No Interface, No Problem: Gesture Recognition on Physical Objects Using Radar Sensing. Sensors, (2021), no. 17: 5771, 20 pages. https://doi.org/10.3390/s21175771

### 
#### usage 
##### training the dataset
```
python ./train.py --dataset_dir human/no_material/full_ds/ --model_type advanced --batch_size 32 --labels_file ./labels.txt --sampling_rate 100 --max_sequence_len 100
```
##### testing 
```
python ./test.py --dataset_dir human/no_material/full_ds/ --model_type advanced --batch_size 32 --labels_file ./labels.txt --sampling_rate 100 --max_sequence_len 100 --model_file human/no_material/full_ds_averaged/advanced_c6b32e100p10l100s100_20211102-030002.h5
```

#### naming convention
```
Naming convention for files: 0_0_1_2_1_4__12_02_2018__14_25_59.soli [trainingLabel][objectId][userId][sessionId][sequenceIndex][testCaseIndex][day][mont][year][hour][min]_[sec]

Naming conventions for Range Doppler images:
0_0_1_2_1_4__12_02_2018__14_25_59/0_0_750.png [name_of_soli_file]/[channel_no][time_instance][frame_id]
```
### extracting zip files
```
# navigate to the appropriate directory then unzip to the same directory
unzip all.zip ./

# concatenate if there are many zip files into one file then extract in the same directory
cat x* > all.zip
unzip all.zip -d ./
```

