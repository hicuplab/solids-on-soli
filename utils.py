#!/usr/bin/env python3
# coding: utf-8

'''
Train a CNN+RNN model to classify user-independent Soli-based microgestures.

External dependencies, to be installed e.g. via pip:
- tensorflow v1.13
- numpy v1.16
- Pillow v6.0

Author: Luis A. Leiva <luis.leiva@aalto.fi>
'''

import sys
import os
import math
import random
import numpy as np
from collections import defaultdict

# Display only TF errors, if any.
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf

# Image shape is `(height, width, channels)`.
# FIXME: Externalize common params to train & test scripts.
IMAGE_SHAPE = (32, 32, 1)

# Make results reproducible.
my_seed = 123456
random.seed(my_seed)
#tf.random.set_seed(my_seed) # TF >= 2 .0
np.random.seed(my_seed)


def basename(f, suffix=None):
    '''
    Bash-like basename implementation.
    >>> basename('/tmp/some.txt')
    'some.txt'
    >>> basename('/tmp/some.txt', '.txt')
    'some'
    '''
    b = os.path.basename(f)
    if suffix:
        return b.split(suffix)[0]
    return b


def h5_filename(prefix, *argv):
    '''
    DEPRECATED!
    Create filename following some conventions, for consistency.
    Such conventions are specified as arguments to this function,
    so you decide what's best for you.
    >>> h5_filename('foo')
    'foo.h5'
    >>> h5_filename('foo@', 1, 2, 3, 'more')
    'foo@1-2-3-more.h5'
    '''
    arg_values = map(str, argv)
    concat_val = '-'.join(arg_values)
    return '{}{}.h5'.format(prefix, concat_val)


def prepare_data_dir(source_dir, target_dir):
    '''
    Parse Soli Doppler files.
    '''
    if os.path.isdir(target_dir) and os.listdir(target_dir):
        print('Dataset already preprocessed. Skipping ...', file=sys.stderr)
        return

    print('Preprocessing images. This may take a while ...', file=sys.stderr)
    if not os.path.isdir(target_dir):
        os.mkdir(target_dir)

    # Example of the expected dir structure of `source_dir`:
    # ~/soliData_15_1_2019/u1_o0/processed/0_0_1_1_0_2__10_02_2018__21_09_04/0_0_750.png
    #   ^base dir          ^user           ^gesture files                    ^ch ^time

    dataset = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list))))
    for root, userdirs, _ in os.walk(source_dir):
        # Here `root` is e.g. `/dat/soliData_15_1_2019/` and `userdirs` is e.g. ['u1_o1', 'u1_o2'].
        for userdir in userdirs:
            # Here `root` is e.g. `/dat/soliData_15_1_2019/u1_o2/`.
            # There is always a `processed` dir inside, so read only that dir.
            for root, gesturedirs, _ in os.walk(os.path.join(root, userdir, 'processed')):
                # Here `root` is e.g. `/dat/soliData_15_1_2019/u1_o2/processed/0_0_1_1_0_2__10_02_2018__21_09_04/`.
                # Actually dir can be `/dat/soliData_15_1_2019/u1_o2/processed/0_000_1_1_000_02/`.
                for gesturedir in gesturedirs:
                    # Ignore spectrogram and beamforming files, see `read_class_labels()` for more info.
                    if not gesturedir.endswith('_s') and not gesturedir.endswith('_b'):
                        # So if dirname is `0_0_1_1_0_2__10_02_2018__21_09_04/` we have:
                        # label_object_user_session_sequence_testcase__day_month_year__hour_min_sec
                        dir_parts = os.path.basename(gesturedir).split('_')
                        label, material, user, sess, seqid, trial = dir_parts[0:6]
                        # Process Doppler files.
                        # Filename format is `channel_frame_time.png`.
                        for gesturedir, _, files in os.walk(os.path.join(root, gesturedir)):
                            # To average all Doppler channels, we have to ensure that each one has the same number of frames.
                            ch_dict = defaultdict(list)
                            for channel in range(4):
                                prefix = '{}_'.format(channel)
                                ch_dict[channel] = sorted([f for f in files if f.startswith(prefix)], key=filesorter)
                            # Now can average all Doppler channels.
                            max_seq_length = min([len(frames) for ch, frames in ch_dict.items()])
                            for frame_num in range(max_seq_length):
                                out_image = average_frames(gesturedir, ch_dict, frame_num)
				# added seqId here, it was giving errors by replacing the same trials
                                out_file = '{}_{}_{:02d}_{:03d}_{:04d}.png'.format(label, user, int(sess), int(seqid)*10 + int(trial), frame_num)
                                if os.path.exists(os.path.join(target_dir, out_file)):
                                    
                                    out_file = '{}_{}_{:02d}_{:03d}_{:04d}.png'.format(label, user, int(sess), int(seqid)*10 + int(trial) + 1, frame_num)
                                if os.path.exists(os.path.join(target_dir, out_file)):
                                    print(out_file)
                                tf.keras.preprocessing.image.save_img(os.path.join(target_dir, out_file), out_image)


def filesorter(filename):
    '''
    Sort files in natural order.
    '''
    # The naming convention for chfiles is: channel_timestep_framenum.png
    channel, time_step, frame = basename(filename, '.png').split('_')
    return int(channel) + int(time_step) + int(frame)


def average_frames(base_dir, ch_dict, frame_num):
    '''
    Average channel frames, to get a robust estimation of the Doppler signal.
    '''
    # Allocate *grayscaled* image space; i.e. 1 channel only.
    avg_image = np.zeros(IMAGE_SHAPE)
    # Soli has 4 channels, but don't hardcode that info here.
    num_channels = len(ch_dict.keys())
    for channel in range(num_channels):
        im_file = ch_dict[channel][frame_num]
        avg_image += load_img(os.path.join(base_dir, im_file))
    avg_image /= num_channels
    return avg_image


def load_img(filepath, color='grayscale'):
    '''
    Load image file as a grayscaled and resized Numpy vector.
    '''
    siz = IMAGE_SHAPE[0:2]
    img = tf.keras.preprocessing.image.load_img(filepath, color_mode=color, target_size=siz)
    return tf.keras.preprocessing.image.img_to_array(img)


def read_class_labels(dirname, labels_file=None):
    # If no labels are provided, use all of them; see `readme.md` for name conventions.
    # Since we work with range Doppler images only, skip the following data:
    # - Spectrogram files, from `*_s` directories.
    # - Beamforming files, from `*_b` directories.
    # - Feature vectors, from `*.csv` files.
    dataset_labels = [os.path.basename(f).split('_')[0] \
        for d, dirs, files in os.walk(dirname) \
            for f in files \
                if not d.endswith('_s') and not d.endswith('_b') and f.endswith('.png')]
    # Ensure that labels are sorted, to make this function deterministic.
    dataset_labels = sorted(set(dataset_labels))
    # If a labels file is provided, use those labels.
    # But ensure that the dataset has actually those labels.
    if labels_file and os.path.isfile(labels_file):
        with open(labels_file) as f:
            user_labels = f.read().splitlines()
        return [label for label in dataset_labels if label in user_labels]
    return dataset_labels


def load_sequence_files(dirname, labels):
    dataset = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(list))))
    # Create dataste structure first, based on filenames.
    for d, _, files in os.walk(dirname):
        for f in files:
            # Format is 'label_user_sess_trial_frame.png'
            if f.endswith('.png'):
                label, user, sess, trial, frame = f.split('_')
                if label in labels:
                    dataset[label][user][sess][trial].append(os.path.join(d, f))
    # Now gather all ordered sequences and labels.
    seq_files, labels = [], []
    for label, users in dataset.items():
        for user, sessions in users.items():
            for sess, trials in sessions.items():
                for trial, files in trials.items():
                    seq_files.append(list(sorted(files)))
                    labels.append(label)

    assert len(seq_files) == len(labels)
    return np.array(seq_files), np.array(labels)


def resample_sequences(seqs, freq):
    # All sequences MUST have the same length at this point.
    seq_len = len(seqs[0])
    if seq_len < freq:
        msg = 'The specified sampling rate ({}) is higher than sequence length ({}), so it will be ignored.'
        print(msg.format(freq, seq_len), file=sys.stderr)
        return np.array(seqs)

    num_steps = math.ceil(seq_len / freq)
    resampled = [ [seq[i] for i in range(0, seq_len, num_steps)] for seq in seqs ]
    return np.array(resampled)


# Define custom image iterators, since built-in ones are for static images only.
# We need to create an image sequence for each gesture.
# TODO: make this function thread-safe to allow multicore processing;
# see https://keras.io/utils/#sequence
def sequence_generator(sequences, labels, batch_size=16, maxlen=None, rate=0, shuffle=True, augmentation=False):
    num_sequences = len(sequences)
    seq_frame_len = max(len(files) for files in sequences)
    if maxlen is not None:
        seq_frame_len = maxlen

    def get_frames(seq_files):
        # Normalize pixel values in the [0, 1] range.
        seq_frames = [load_img(f)/255. for f in seq_files]
        # By default read the full sequence.
        rand_ini = 0
        rand_end = seq_frame_len
        if augmentation:
            droprate = 0.1
            rand_ini = np.random.randint(0, droprate * seq_frame_len)
            rand_end = np.random.randint((1 - droprate) * seq_frame_len, seq_frame_len)
        return seq_frames[rand_ini:rand_end]

    while True:

        if shuffle:
            X, y = [], []
            # Pick some sequences at random from the pool of candidates.
            indices = np.random.randint(num_sequences, size=batch_size)
            for i in indices:
                seq_files, seq_label = sequences[i], labels[i]
                seq_frames = get_frames(seq_files)
                X.append(seq_frames)
                y.append(seq_label)

            X, y = np.array(X), np.array(y)
            if rate > 0:
                # Ensure all sequences have the same length before resampling.
                X = tf.keras.preprocessing.sequence.pad_sequences(X, seq_frame_len)
                X = resample_sequences(X, rate)

            X = tf.keras.preprocessing.sequence.pad_sequences(X, seq_frame_len)
            yield X, y

        else:
            # For prediction don't shuffle data, to prevent label mismatches.
            for offset in range(0, num_sequences, batch_size):
                X, y = [], []
                sequences_batch = sequences[offset:offset+batch_size]
                labels_batch = labels[offset:offset+batch_size]
                for i, seq in enumerate(sequences_batch):
                    seq_files, seq_label = sequences_batch[i], labels_batch[i]
                    seq_frames = get_frames(seq_files)
                    X.append(seq_frames)
                    y.append(seq_label)

                X, y = np.array(X), np.array(y)
                if rate > 0:
                    # Ensure all sequences have the same length before resampling.
                    X = tf.keras.preprocessing.sequence.pad_sequences(X, seq_frame_len)
                    X = resample_sequences(X, rate)

                X = tf.keras.preprocessing.sequence.pad_sequences(X, seq_frame_len)
                yield X, y


# Count the number of instances per class, since it might happen that data partitions are incomplete;
# e.g. some classes may be missing in training/validation/test and thus the model won't learn.
# NOTE: labels are assumed to be one-hot encoded.
def label_histogram(onehot_labels, mapping=None):
    d = defaultdict(int)
    for l in onehot_labels:
        c = np.argmax(l)
        if mapping is not None:
            c = mapping[c]
        d[c] += 1
    return dict(sorted(d.items()))


# Encode labels in an deterministic manner.
def encode_labels(labels, classes):
  classes = sorted(classes)
  mapping = {x:i for i,x in enumerate(classes)}
  classes = [mapping[c] for c in classes]
  onehots = tf.keras.utils.to_categorical(classes, num_classes=len(classes))
  encdict = dict(zip(classes, onehots))
  # Return both the encoded label and the (reversed) mapping between class (string) names and map (int) indices.
  return np.array([encdict[mapping[label]] for label in labels]), {v:k for k,v in mapping.items()}


def create_model(in_shape, out_shape, type='basic'):
    # NB: `in_shape`is (Batch size, Sequence length, Img height, Img width, Num channels)
    model = tf.keras.Sequential()

#    # The input layer is common to both models.
#    # If we want a stateful LSTM, we must specify the batch size in the input layer.
#    model.add(tf.keras.layers.InputLayer(input_shape=in_shape[1:], batch_size=in_shape[0]))
#    # NB: There is a bug in TF < 2.0 that doesn't save properly the models that have an InputLayer.
#    # See more at https://github.com/tensorflow/tensorflow/issues/26809 and https://github.com/keras-team/keras/issues/10417

    if type == 'basic':

        # Quick test: Try the built-in ConvLSTM layer. Spoiler: Doesn't work great.
        model.add(tf.keras.layers.ConvLSTM2D(128, 3, activation='relu', padding='same', batch_input_shape=in_shape))
        model.add(tf.keras.layers.Dropout(rate=0.25))
        model.add(tf.keras.layers.Flatten())
        #model.add(tf.keras.layers.Dense(128, activation='relu'))

    elif type == 'advanced':

        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Conv2D(32, 3, activation='relu', padding='same'), batch_input_shape=in_shape))

        # Add convolutional block.
        conv_filters = [32, 64, 128]
        for index, filter_size in enumerate(conv_filters):
            model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Conv2D(filter_size, 3, activation='relu', padding='same')))
            model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.MaxPooling2D(2, strides=2)))
            model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.SpatialDropout2D(rate=0.25)))

        # Add recurrent block.
        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Flatten()))
        model.add(tf.keras.layers.LSTM(conv_filters[-1]*2, dropout=0.25, recurrent_dropout=0.25))

#        # Optionally add fully connected layers, to increase the model capacity,
#        # thought it may be prone to overfitting.
#        model.add(tf.keras.layers.Dense(conv_filters[-1]*2, activation='relu'))
#        model.add(tf.keras.layers.Dense(conv_filters[-1], activation='relu'))
#        model.add(tf.keras.layers.Dropout(rate=0.25))

#        # OLD code, without LSTM block.
#        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(conv_filters[-1], activation='relu')))
#        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(conv_filters[-1], activation='relu')))
#        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Dropout(rate=0.5)))
#        #model.add(tf.keras.layers.Flatten())
#        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Flatten()))
#        model.add(tf.keras.layers.LSTM(conv_filters[-1], dropout=0.25))

    # The output layer is common to both models.
    model.add(tf.keras.layers.Dense(out_shape[1], activation='softmax'))
    return model


