import React, { Component } from "react";
import {
  Grid,
  Segment,
  Dropdown,
  Divider,
  Header,
  Icon,
  Image,
  Container
} from "semantic-ui-react";

import "./dropdown.css";
class Dropdowns extends Component {
  render() {
    const squareButtonStyle = {
      borderRadius: "4px",
    };

    const roundedButtonStyle = {
      borderRadius: "32px",
    };

    return (
      <Grid columns="equal">
        <Grid.Row>
          <Grid.Column width={16}>
            {/* <Segment> */}
            <Header as="h2" icon textAlign="center" style={{ paddingTop: '20px' }}>
              {/* <Icon name="wave-square" circular /> */}
              <Header.Content>How to use the catalogue</Header.Content>
            </Header>

            {/* </Segment> */}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Container text style={{ textAlign: "left" }}>
              <Header as='h3'>
                {'1.) Simple usage:'}
              </Header>
              <p>
                If you would like to browse suitability of materials based on models trained by us just run the
                catalogue page (<a href='#'>https://solidsonsoli.famnit.upr.si/catalogue</a>). Be patient. It takes some time to load.
                You can choose between two different models we tested (CNN+LSTM or Conv3D). After loading you should see something like this.
              </p>
              <Image
                style={{ width: '1000px' }}
                centered
                size="large"
                src="https://firebasestorage.googleapis.com/v0/b/solidsonsoli.appspot.com/o/imgs%2Fsolidsonsoli1.png?alt=media&token=b515d223-82c6-46a9-8229-0ecb6c785b77"
              />
            </Container>
          </Grid.Column>

        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <Container text style={{ textAlign: "left" }}>
              <Header as='h3'>
                {'2.) Advance usage:'}
              </Header>
              <p>
                If you would like to browse suitability of materials based on data from your own models, you need to:
              </p>

              <p>
              <li>
                  <span style={{ fontWeight:'bold'}} >Step 1: </span>
                  Choose a minimum of 3 materials from the catalogue (the thickness should also match!)
                  and test the performance of your recogniser as you are trying to sense gestures through them.
                  The performance metric we suggest is (Accuracy + AUC)/2.
                </li>
                <li>
                  <span style={{ fontWeight:'bold'}} >Step 2: </span>
                  Select Custom and enter the performance data for at least 3 materials.
                </li>
                <li>
                  <span style={{ fontWeight:'bold'}} >Step 3: </span>
                  After the model is built you should be able to browse  through materials and should see something like this:
                </li>
              </p>
              <Image
                style={{ width: '1000px' }}
                centered
                size="large"
                src="https://firebasestorage.googleapis.com/v0/b/solidsonsoli.appspot.com/o/imgs%2FScreenshot%20from%202022-06-21%2021-00-57.png?alt=media&token=7b99c8c1-b536-4a5c-a690-499d779d96a3"
              />
            </Container>
          </Grid.Column>

        </Grid.Row>
      </Grid>
    );
  }
}

export default Dropdowns;
