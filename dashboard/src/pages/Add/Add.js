import React from 'react'
import { Header, Icon, Image, Container } from 'semantic-ui-react'
// import SinglePagePDFViewer from "../../components/PdfViewer/SinglePage"

// import samplePDF from "./solids_on_soli.pdf";

// import PdfViewer from '../../components/PdfViewer/PdfViewer'
// import './home.css'

const Add = (props) => {

    return (<div>
        <Header as='h2' icon textAlign='center'>
            <Icon name='plus' circular />
            <Header.Content>Expanding the catalogue</Header.Content>
        </Header>
        <Container>
            <p textAlign='center'>
                This web application consits of a react front-end and a nodejs backend. 
                To expand the catalogue you need to run the front-end and backend on your own instances. Refer below postman collection for posting new material data into the catalogue. 
                <a href='https://www.getpostman.com/collections/fd2f0c1aa9e99f2ce931'>https://www.getpostman.com/collections/fd2f0c1aa9e99f2ce931</a>
            </p>
        </Container>

    </div>)
    // return (<SinglePagePDFViewer pdf={samplePDF} />)
    // return (<PdfViewer pdf={samplePDF} />)
}

export default Add;