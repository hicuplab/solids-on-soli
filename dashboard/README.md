### Solids on Soli dashboard

demo : https://solidsonsoli.famnit.upr.si/
![enter image description here](assets/solidsonsoli.famnit.upr.si.png)

Please use the build.zip for easy deploy of the application.
- unzip `build.zip` to a directory
- install serve
  - ``` npm install -g serve```
- run serve in a specific port
  - ``` serve -l 5000```


Please execute the following commands for launching the dev server
```
npm install # for installing all the dependencies
npm run start # this command will initialize the start script

```
Run below for building the application
```
npm run build
```

