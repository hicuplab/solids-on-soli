#!/usr/bin/env python3
# coding: utf-8

'''
Test a CNN+RNN model to classify user-independent Soli-based microgestures.

External dependencies, to be installed e.g. via pip:
- tensorflow v1.13
- numpy v1.16
- Pillow v6.0

Author: Luis A. Leiva <luis.leiva@aalto.fi>
'''

import sys
import os
import random
import math
import numpy as np
import argparse

# Define CLI options.
parser = argparse.ArgumentParser(description='Test gesture classifier on Soli data.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--dataset_dir', required=True, help='path to recorded soli data directory')
#parser.add_argument('--dataset_files', nargs='+', help='path to individual soli files. DEBUG ONLY')
parser.add_argument('--model_file', required=True, help='trained model file')
parser.add_argument('--model_type', choices=['basic', 'advanced'], help='model architecture')
parser.add_argument('--batch_size', type=int, default=32, help='batch size')
parser.add_argument('--labels_file', help='path to labels file, one label per line')
parser.add_argument('--sampling_rate', type=int, default=0, help='sampling rate, in Hz')
parser.add_argument('--max_sequence_len', type=int, help='max length for all sequences, in timesteps')
parser.add_argument('--test_split_size', type=float, default=0.3, help='test set size, relative to the train set')

args = parser.parse_args()
print(args, file=sys.stderr)

# Display only TF errors, if any.
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
import tensorflow as tf
from utils import IMAGE_SHAPE, create_model, prepare_data_dir, read_class_labels, load_sequence_files, sequence_generator, label_histogram, encode_labels
from sklearn.metrics import accuracy_score, precision_recall_fscore_support, roc_auc_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize

# Boost GPU usage.
conf = tf.compat.v1.ConfigProto()
conf.gpu_options.allow_growth = True
tf.compat.v1.Session(config=conf)

# Makes results reproducible.
my_seed = 123456
random.seed(my_seed)
#tf.random.set_seed(my_seed) # TF >= 2 .0
np.random.seed(my_seed)

# FIXME: Externalize common params so that train & test scripts can read the same info.
DATASET_AVG = os.path.normpath(args.dataset_dir) + '_averaged'
prepare_data_dir(args.dataset_dir, DATASET_AVG)

CLASS_LABELS = read_class_labels(DATASET_AVG, args.labels_file)
NUM_CLASSES = len(CLASS_LABELS)

sequences, orig_labels = load_sequence_files(DATASET_AVG, CLASS_LABELS)

# Convert labels to one-hot encodings, for which we need to convert labels (strings) to integers.
# Ensure that we assign the encodings as in `train.py`.
labels, mapping = encode_labels(orig_labels, CLASS_LABELS)

# When testing gestures on new materials we can specify a target dir with *only* new data,
# so actually create partitions if the given test size is less than 100% (1.0).
if args.test_split_size < 1.0:
    X_train, X_test, y_train, y_test = train_test_split(sequences, labels, test_size=args.test_split_size, random_state=my_seed, stratify=labels)
else:
    X_test, y_test = sequences, labels

# Compute hash to verify that partitions are created the same way both in train.py and test.py
print('Test partition hash:', hash(str(X_test)))

# -- BEGIN count ---
label_hist = label_histogram(labels, mapping)

print('-'*80)
print('All labels :', label_hist)
print('Test       :', label_histogram(y_test, mapping))
print('-'*80)

print('Will classify {} classes.'.format(NUM_CLASSES))
actual_num_classes = len(label_hist.keys())
if actual_num_classes != NUM_CLASSES:
    NUM_CLASSES = actual_num_classes
    print('Actually will classify {} classes.'.format(NUM_CLASSES))
# -- END count ---

# Ensure that *all* sequences have the same length, in all generators.
seq_frame_len = max(len(seq) for seq in sequences)

if args.max_sequence_len:
    seq_frame_len = args.max_sequence_len

# Setup the data generators. For making predictions DON'T shuffle the data.
test_generator = sequence_generator(X_test, y_test, shuffle=False,
  batch_size=args.batch_size, maxlen=seq_frame_len, rate=args.sampling_rate)

model = tf.keras.models.load_model(args.model_file, compile=False)
# If we specify the model type, we can change batch size on the fly by sharing model weights.
# This is *very* useful to predict 1 gesture at a time,
# which is essentially how we would use the model in production.
if args.model_type:
    train_weights = model.get_weights()
    input_shape = (args.batch_size, seq_frame_len,) + IMAGE_SHAPE 
    model = create_model(input_shape, labels.shape, type=args.model_type)
    model.compile(optimizer='adam', loss='categorical_crossentropy')
    model.set_weights(train_weights)

# Print model to be sure we're using the right one.
#model.summary()

y_pred = model.predict_generator(
    test_generator,
    workers=0, # TODO: Make generator thread-safe and set `use_multiprocessing=True`.
    steps=len(X_test) // args.batch_size or 1,
)

n_preds = len(y_pred)
n_trues = len(y_test)
# Sometimes the last batch is not complete, so equalize the number of samples.
# NB: This can be solved by specifiying the right `batch_size` and `model_type` args.
if n_preds < n_trues:
    y_test = y_test[:n_preds]
    print('NOTICE: Removed {} true samples because of batch size mismatch. Will test with {} samples in total.'.format(n_trues - n_preds, n_preds), file=sys.stderr)

# Retrieve actual labels.
pred_labels = np.argmax(y_pred, axis=1)
true_labels = np.argmax(y_test, axis=1)

print('---')
print('Accuracy: {:.2f}%'.format(100*accuracy_score(true_labels, pred_labels)))
prec, rec, f1, _ = precision_recall_fscore_support(true_labels, pred_labels, average='weighted')
print('Precision / Recall / F1: {:.2f}% / {:.2f}% / {:.2f}%'.format(100*prec, 100*rec, 100*f1))

## In sklearn we can't have sparse int labels when computing ROC,
## so re-encode them as one-hot vectors to remove label sparsity.
#class_labels = list(sorted(map(int, set(orig_labels))))
#print(class_labels)
#_pred_labels = label_binarize(pred_labels, classes=class_labels)
#_true_labels = label_binarize(true_labels, classes=class_labels)
#print(_pred_labels)
#print(_true_labels)
#print('AUC: {:.2f}%'.format(100*roc_auc_score(_true_labels, _pred_labels, average='weighted', labels=class_labels, multi_class='ovo')))

# NEW: Manual way of dealing with class names that look like integers.
class_labels = sorted(list(set(pred_labels)))
_true_labels, _ = encode_labels(true_labels, class_labels)
_pred_labels, _ = encode_labels(pred_labels, class_labels)
print('AUC: {:.2f}%'.format(100*roc_auc_score(_true_labels, _pred_labels, average='weighted', multi_class='ovo')))

# The confusion matrix works fine with sparse int labels.
print(confusion_matrix(true_labels, pred_labels))
