BUFFER_SIZE = 1478
# The batch size of 1 produced better results for the U-Net in the original pix2pix experiment
BATCH_SIZE = 1
# Each image is 3x400 in size
IMG_WIDTH = 400
IMG_HEIGHT = 3

OUTPUT_CHANNELS = 1

LEARNING_RATE = 0.0002