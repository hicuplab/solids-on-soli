'''
Author: Nuwan T. Attygalle
email: nuwan.attygalle@famnit.upr.si
'''
# %%
from header import *
# %%
no_classes = 6
batch_size = 32
no_epochs = 200
verbosity = 1
patience = 50
learning_rate = 0.0002
sample_shape = (400, 32, 32, 3)
tcSortedFilteredUris = [i for i in range(6)]

# %%

def getModel():
    model = Sequential()
    model.add(Conv3D(32, kernel_size=(2, 2, 2), activation='LeakyReLU', kernel_initializer='he_uniform', input_shape=sample_shape))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(Dropout(0.5))
    model.add(Conv3D(64, kernel_size=(2, 2, 2), activation='LeakyReLU', kernel_initializer='he_uniform'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(Dropout(0.5))
    model.add(Conv3D(128, kernel_size=(2, 2, 2), activation='LeakyReLU', kernel_initializer='he_uniform'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(Dropout(0.5))
    model.add(Conv3D(256, kernel_size=(2, 2, 2), activation='LeakyReLU', kernel_initializer='he_uniform'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(512, activation='LeakyReLU', kernel_initializer='he_uniform'))
    model.add(Dense(no_classes, activation='softmax'))
    return model

for gestureDirs in [[0, 1, 2, 3, 4, 5]]: # [[1, 2, 3, 4, 5], [0, 1, 2, 3, 4], [0, 1, 2, 3, 5]]:

    tcSortedFilteredUris = [i for i in range(6)]
    for label, gesture in enumerate(gestureDirs):
        print(label, gesture)
#         break
        gestureFolders = glob.glob('../robot/no_material/{:03d}_averaged/{:d}/*'.format(0, gesture))
        
        gestureFolders = random.sample(gestureFolders, 150)
#         print(len(gestureDirs), gestureDirs)
        tcSortedFilteredUris[label] = gestureFolders
    
    l0 = getImagesForEachMat(0, tcSortedFilteredUris)
    l1 = getImagesForEachMat(1, tcSortedFilteredUris)
    l2 = getImagesForEachMat(2, tcSortedFilteredUris)
    l3 = getImagesForEachMat(3, tcSortedFilteredUris)
    l4 = getImagesForEachMat(4, tcSortedFilteredUris)
    l5 = getImagesForEachMat(5, tcSortedFilteredUris)
    
    trainl0y = np.full(len(l0), 0)
    trainl1y = np.full(len(l1), 1)
    trainl2y = np.full(len(l2), 2)
    trainl3y = np.full(len(l3), 3)
    trainl4y = np.full(len(l4), 4)
    trainl5y = np.full(len(l5), 5)
    
    trainlx = np.array(l0 + l1 + l2 + l3 + l4 + l5)
    trainly = np.concatenate((trainl0y, trainl1y, trainl2y, trainl3y, trainl4y, trainl5y))

    trainlx = tf.keras.preprocessing.sequence.pad_sequences(
    trainlx, padding="post", dtype="float32", maxlen=400)

    targets_train = to_categorical(trainly).astype(np.int32)
    X_train, X_val, y_train, y_val = train_test_split(trainlx, targets_train, test_size=0.285714286, random_state=42)

    print(X_train.shape, X_val.shape)
    trainData = tf.data.Dataset.from_tensor_slices((X_train, y_train))
    valData = tf.data.Dataset.from_tensor_slices((X_val, y_val))
    # testData = tensorflow.data.Dataset.from_tensor_slices((testx, targets_test))
    print(trainData)
    print(valData)
    SHUFFLE_BUFFER_SIZE = 100

    trainData = trainData.shuffle(SHUFFLE_BUFFER_SIZE).batch(batch_size)
    valData = valData.batch(batch_size)

    
    model = getModel()
    visualizer = tf.keras.callbacks.TensorBoard(log_dir='./tfevents_conv3d_trained_on_noMat_{}_{}'.format(len(gestureDirs), "-".join(str(i) for i in gestureDirs)))
    filepath = './conv3d_trained_models_no_mat/conv3d_trained_on_noMat_{}_{}.h5'.format(len(gestureDirs), "-".join(str(i) for i in gestureDirs))
    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, \
                         save_best_only=True, save_weights_only=False, \
                         mode='auto', save_frequency=1)        
    earlystops = tf.keras.callbacks.EarlyStopping(patience=patience, restore_best_weights=True, verbose=1)#, monitor='val_acc', mode='max')

    # Compile the model
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer=tf.keras.optimizers.Adam(lr=learning_rate),
                  metrics=['accuracy'])
    history = model.fit(trainData,
                batch_size=batch_size,
                epochs=no_epochs,
                verbose=verbosity,
                validation_data=valData,
                callbacks=[visualizer, earlystops, checkpoint])    

    

# %%
