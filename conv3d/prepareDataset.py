'''
Author: Nuwan T. Attygalle
email: nuwan.attygalle@famnit.upr.si
Prerequisites: extract the compressed files into appropriate directories
               e.g. ../robot/no_material/full_ds_averaged/
               Here we'll get the extracted dataset from ../robot/full_ds_averaged/
'''
# %%
from header import *
# %%
dir_nomat = '../robot/no_material/'
for i in range(0, 1): # range(1, 83):
    filenames = [os.path.basename(x) for x in glob.glob(dir_nomat + '{:03d}_averaged/*'.format(i))]
#     for ges in range(6):
#         os.system('mkdir -p ' + x + '{:03d}_averaged/{:d}/'.format(i, ges))
    for file in filenames:
        folder = file[:11]
        ges = file[:1]
        os.system('mkdir -p ' + dir_nomat + '{:03d}_averaged/{:s}/'.format(i, ges) + folder)
        os.system('mv ' + dir_nomat + '{:03d}_averaged/'.format(i) + file + ' ' + dir_nomat + '{:03d}_averaged/{:s}/'.format(i, ges) + folder + '/')
#         break
#     print(filenames)
#     break
# %%
