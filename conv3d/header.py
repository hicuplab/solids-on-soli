# clusters from acc vs tc

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Conv3D, MaxPooling3D, Dropout, BatchNormalization, SpatialDropout3D
from tensorflow.keras.utils import to_categorical
import h5py
import numpy as np
import matplotlib.pyplot as plt
import glob
import os
# from PIL import Image
import PIL
from random import shuffle

from sklearn.model_selection import train_test_split

# from sklearn.metrics import confusion_matrix
import seaborn as sns
import pandas as pd
from datetime import datetime
from tensorflow import keras
from numba import cuda
import random

from shutil import copyfile, move
import threading
from sklearn.metrics import accuracy_score, precision_recall_fscore_support, roc_auc_score, confusion_matrix, silhouette_samples, silhouette_score

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

from tensorflow.keras.callbacks import ModelCheckpoint


import random


from const import BUFFER_SIZE, BATCH_SIZE, IMG_WIDTH, IMG_HEIGHT, OUTPUT_CHANNELS, LEARNING_RATE
from PIL import Image
from PIL import ImageFile
from sklearn.preprocessing import MinMaxScaler
ImageFile.LOAD_TRUNCATED_IMAGES = True
import argparse
from utils import *
