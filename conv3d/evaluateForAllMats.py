'''
Author: Nuwan T. Attygalle
email: nuwan.attygalle@famnit.upr.si
        Evaluate the accuracy of the trained model on no material condition against the gestures performed through materials in the catelogue. 
'''


from header import *

tcSortedFilteredUris = [i for i in range(6)]

for gestureDirs in [[0, 1, 2, 3, 4, 5]]: # [[1, 2, 3, 4, 5], [0, 1, 2, 3, 4], [0, 1, 2, 3, 5]]:
    
    filepath = './conv3d_trained_models_no_mat/conv3d_trained_on_noMat_{}_{}.h5'.format(len(gestureDirs), "-".join(str(i) for i in gestureDirs))
    
    predictAccAuc = []
    print('loading the best model from disk')
    model = tf.keras.models.load_model(filepath)
    for v in range(1,83):
        val = v
        if v == 11 or v == 22 or v == 23 or v == 65 or v == 74 or v == 81 or v == 25 or v == 71:
            predictAccAuc.append([v, 0, 0])
            continue
            # testing rest data
        tcSortedFilteredUris = [i for i in range(6)]
        for label, gesture in enumerate(gestureDirs):
#             print(label, gesture)
    #         break
            gestureFolders = glob.glob('../robot/through_materials/{:03d}_averaged/{:d}/*'.format(v, gesture))

            gestureFolders = random.sample(gestureFolders, 20)
    #         print(len(gestureDirs), gestureDirs)
            tcSortedFilteredUris[label] = gestureFolders

        l0 = getImagesForEachMat(0, tcSortedFilteredUris)
        l1 = getImagesForEachMat(1, tcSortedFilteredUris)
        l2 = getImagesForEachMat(2, tcSortedFilteredUris)
        l3 = getImagesForEachMat(3, tcSortedFilteredUris)
        l4 = getImagesForEachMat(4, tcSortedFilteredUris)
        l5 = getImagesForEachMat(5, tcSortedFilteredUris)

        trainl0y = np.full(len(l0), 0)
        trainl1y = np.full(len(l1), 1)
        trainl2y = np.full(len(l2), 2)
        trainl3y = np.full(len(l3), 3)
        trainl4y = np.full(len(l4), 4)
        trainl5y = np.full(len(l5), 5)

        trainlx = np.array(l0 + l1 + l2 + l3 + l4 + l5)
        trainly = np.concatenate((trainl0y, trainl1y, trainl2y, trainl3y, trainl4y, trainl5y))

        testx = tf.keras.preprocessing.sequence.pad_sequences(
        trainlx, padding="post", dtype="float32", maxlen=400)

        targets_test = to_categorical(trainly).astype(np.int32)
        
        print(testx.shape, targets_test.shape)
        try: 
            loss, acc = model.evaluate(testx, targets_test, verbose=1)
            predict = model.predict(testx, 6)
        #     predict = np.round(predict, decimals=0)
            y_score = to_categorical(predict.argmax(1), num_classes=6).astype(np.int32)
    #         print(y_score)
            labels = [0, 1, 2, 3, 4, 5]
        #     labels = [0, 1, 2, 3, 4]
        #     cm = confusion_matrix(targets_test.argmax(axis=1), predict.argmax(axis=1), labels)
            y_true = to_categorical(targets_test.argmax(1), num_classes=6).astype(np.int32)
        #     print(y)

        #     cm = confusion_matrix(y, x, labels)
            # print(cm)
        #     vvv = mmm.update_state(y_true=targets_test, y_pred=predict, sample_weight=None)
            # auc = mmm.interpolate_pr_auc().numpy()
    #             try:
            auc = 100*roc_auc_score(y_true, y_score , average='weighted', multi_class='ovo')
            acc = 100*accuracy_score(y_true, y_score)
            print(' {:03d} => Test loss: {:.5f}, Test accuracy: {:.5f}, AUC {:.5f}\n'.format( val, loss, acc, auc))
            predictAccAuc.append([v, acc, auc])
        except:
            print("An exception occurred " + str(v))
            print('EXCEPTION ')
            print(testx.shape)
#             print(testy.shape)
            predictAccAuc.append([v, "-", "-"])

        predictAccAuc1 = np.asarray(predictAccAuc)
        np.savetxt('conv3d_trained_on_noMat_{}_mat_gestures_{}.csv'.format(0, "-".join(str(i) for i in gestureDirs)), predictAccAuc1,  fmt='%s')   
