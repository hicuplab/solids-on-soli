'''
support functions
Authors:
- Nuwan T. Attygalle <nuwan.attygalle@famnit.upr.si
'''
# %%
from header import *
from sklearn.preprocessing import MinMaxScaler
from PIL import Image
from PIL import ImageFile
# %%
def getNoOfFilesToBeSelected(dirs, call):
    noOfGestures = 630/(len(dirs)*6)
    if(noOfGestures-int(noOfGestures)> 0):
#         call = call + 1
        if(call <= (630-(int(noOfGestures)*len(dirs)*6))):
            return int(noOfGestures + 1)
        return int(noOfGestures)
    return int(noOfGestures)
def getImagesForEachMat(label, tcSortedFilteredUris):
#     print('came here')
    dataArray = []
    for i in [label]: #, 3, 6, 9, 10]:
        
        dirs = tcSortedFilteredUris[label]
        for folName in dirs:
            tempImages = []
#             print('folname ', folName)
            images = [x for x in glob.glob( folName + '/*')]
            if(len(images) == 0):
                print(folName)
#             print(images)
#             if(len(images) > 82):
            for i, img in enumerate(sorted(images)):
#                 if(i < 82):
#                 print(img)
                img = getImgNp(img)
                img = array_to_color(img)
                tempImages.append(img)
            tempImages = np.array(tempImages)
            dataArray.append(tempImages)
            
    return dataArray
# c = []
def array_to_color(array, cmap="Oranges"):
    cmap = plt.cm.viridis
    s_m = plt.cm.ScalarMappable(cmap=cmap)
    image = s_m.to_rgba(array, alpha=None, bytes=False, norm=True)
    
    return image[:,:,:-1]

def rgb_data_transform(data):
    data_t = []
    for i in range(data.shape[0]):
        data_t.append(array_to_color(data[i]).reshape(78, 32, 32, 4))
    return np.asarray(data_t, dtype=np.float32)

def getPic(img_path):
    return np.array(Image.open(img_path).convert('RGB'))
def getImgNp(uri):
    img = np.array(Image.open(str(uri)));
#     print('img shape ')
#     print(img.shape)
    return img
#     print(img.shape)
# import random
# %%
def csvToImg(file, directory):
    df = pd.read_csv(file, sep='\t')
    filename = os.path.basename(file).replace('csv', 'png')
    fig1, ax1 = plt.subplots()
    ax1.axis('off')
    ax1.scatter(df[' x'], df[' y'], c=df[' time'])
    fig1.savefig(directory + '/' + filename, dpi=1000)
    plt.close(fig1)

def convertDatasetToImages():
    PATH = '../dataset/'
    folders = ['naive/', 'realTO/', 'recoTO/', 'syntTO/']
    files = glob.glob(PATH + folders[0] + '/*')
    for file in files:
        # for folder in folders:
        f = os.path.basename(file)
        csvToImg(PATH + folders[0] + f, '../img_dataset/' + folders[0] )
        csvToImg(PATH + folders[1] + f, '../img_dataset/' + folders[1] )
        csvToImg(PATH + folders[2] + f, '../img_dataset/' + folders[2] )
        csvToImg(PATH + folders[3] + f, '../img_dataset/' + folders[3] )

# %%
# get train, val and test partitions file names

def getRest(fullArray, selected):
    return list(set(fullArray) - set(selected))
# gives the names of the files of train test val partitions
# usage
# trainNames, testNames, valNames = getTrainTestValNames(path)
def getTrainTestValNames(path):
    allFilesNames = [os.path.basename(f).replace('.csv', '') for f in glob.glob( path + '*.csv')]
    trainNames = random.sample(allFilesNames, int(len(allFilesNames)*.7))
    valNames = random.sample(getRest(allFilesNames, trainNames), int(len(allFilesNames)*.1))
    testNames = random.sample(getRest(allFilesNames, [*trainNames, *valNames]), int(len(allFilesNames)*0.2))

    return trainNames, testNames, valNames

# %%
# resize images
def resizeImg(img):
    a = np.asarray(img)
    resized = tf.image.resize(img, [int(a.shape[0]/10), int(a.shape[1]/10)], method='bilinear')
    # resized = np.asarray(tf.cast(resized, tf.int32).numpy()).astype(np.float)
    return resized
def resizeAndSaveImage(fileNames, targetDir):
    for name in fileNames:
        print(name)
        naive = tf.keras.preprocessing.image.load_img('../img_dataset/naive/' + name + '.png')
        real = tf.keras.preprocessing.image.load_img('../img_dataset/realTO/' + name + '.png')
        reco = tf.keras.preprocessing.image.load_img('../img_dataset/recoTO/' + name + '.png')
        synt = tf.keras.preprocessing.image.load_img('../img_dataset/syntTO/' + name + '.png')

        naive = resizeImg(naive)
        real = resizeImg(real)
        reco = resizeImg(reco)
        synt = resizeImg(synt)
        
        # plt.imsave(naive, targetDir + 'naive/' + name + '.png')
        # naive.save(targetDir + 'naive/' + name + '.png')
        # generic_utils.py
        tf.keras.utils.save_img(
            targetDir + 'naive/' + name + '.png', naive, scale=True
        )
        tf.keras.utils.save_img(
            targetDir + 'realTO/' + name + '.png', real, scale=True
        )
        tf.keras.utils.save_img(
            targetDir + 'recoTO/' + name + '.png', reco, scale=True
        )
        tf.keras.utils.save_img(
            targetDir + 'syntTO/' + name + '.png', synt, scale=True
        )
        # break

# %%
# # creates compressed datafiles for train, test and validation partitions.
# # and saves it
## reads and returns data with/out scaling
# from sklearn.preprocessing import MinMaxScaler
def getDataFromCsv(file, scale):
  # read the datafile
  df = pd.read_csv(file, sep='\t')
  data = df[[' x', ' y', ' time']].values
  if(scale):
    scalar = MinMaxScaler()
    data = scalar.fit_transform(data)
  # transpose the data into the format 400, 3
  data = np.transpose(data)
  data = tf.keras.preprocessing.sequence.pad_sequences(data, maxlen=400, dtype='float32')
  return data

# call 
# @ usage
# createCompressedDatafile(trainNames, testNames, valNames, scale=False, outPath='../npz/csv/csv')
# createCompressedDatafile(trainNames, testNames, valNames, scale=True, outPath='../npz/csv/csv_scaled')
# @ returns
# data = np.load('../npz/csv/csvtrain.npz')
# naive, reco, synt = data['y']
# real = data['x']
def createCompressedDatafile(train, test, val, scale, csvPath, outPath):
    # csvPath = '../dataset/'
    
    datasetDirs = ['naive/', 'realTO/', 'recoTO/', 'syntTO/']
    # read the csv s
    for ds in [train, test, val]:
      naive, real, reco, synt = [], [], [], []
      for file in ds:
        na = getDataFromCsv(csvPath + datasetDirs[0] + file + '.csv', scale)
        rea = getDataFromCsv(csvPath + datasetDirs[1] + file + '.csv', scale)
        rec = getDataFromCsv(csvPath + datasetDirs[2] + file + '.csv', scale)
        sy = getDataFromCsv(csvPath + datasetDirs[3] + file + '.csv', scale)

        naive.append(na)
        real.append(rea)
        reco.append(rec)
        synt.append(sy)

      x = np.asarray(real, dtype=np.float32)
      y = np.asarray([naive, reco, synt], dtype=np.float32)
      if(len(list(set(ds) - set(train) )) == 0):
        np.savez(outPath + 'train.npz', x=x, y=y)
      if(len(list(set(ds) - set(test))) == 0):
        np.savez(outPath + 'test.npz', x=x, y=y)
      if(len(list(set(ds) - set(val))) == 0):
        np.savez(outPath + 'val.npz', x=x, y=y)

# get train, test, val dataset real, naive, 
# naive, reco, synt
# choose naive, reco, synt or real as train,
# always target is real
# '../npz/csv/csv_scaledtrain.npz'
def getDataset(url, dataset):
  data = np.load(url)
# data = np.load('../npz/csv/csvtrain.npz')
  if(dataset == 'naive'):
    train, _, _ = data['y'] # naive
  if(dataset == 'reco'):
    _, train, _ = data['y'] # reco
  if(dataset == 'synt'):
    _, _, train = data['y'] # synt
  if(dataset == 'real'):
    train = data['x'] # if we want real as the target as well. 

  target = np.asarray([i.reshape(3, 400, 1) for i in data['x']]) # real
  train = np.asanyarray([i.reshape(3, 400, 1) for i in train])

  return train, target

# get the filenames for the train, test, validation partitions
# @ usage train, test, val = getTrainTestValFileNames('../npz/chosenFileNames/names.csv')
# @ return train, test, val => list of names of the files
def getTrainTestValFileNames(file):

    names = pd.read_csv(file)
    train = names.iloc[0][1:1479].values
    test = names.iloc[1][1:423].values
    val = names.iloc[2][1:212].values

    return train, test, val

# @ summary format csv and saves to disc
# @ params data => csv with x, y, time, 
# @ params path => path to save the file
# @ return non
def saveCSV(data, path):
    # adding two columns
    # if not isinstance(data, pd.DataFrame):
    #   data = pd.DataFrame(data, columns=[' x', ' y', ' time'])
    data[' stroke_id'] = 1.0
    data[' is_writing'] = 1.0
    # sort the columns
    data = data[[' stroke_id', ' x', ' y', ' time', ' is_writing']]
    # sort values by time ascending
    # data.sort_values(by=' time', ascending=True, inplace=True)
    # remove the nan values
    data.dropna(inplace=True)
    # write to disc
    data.to_csv(path, sep='\t', index=False)

# @ usage => creates delta csv
# @ param => csv with columns x, y, time
# @ return => delta csv
def getDeltaCSV(csv):

    delta = []
    prev = []
    for i, row in csv.iterrows():
        if(i == 0):
            prev = row
            continue
        diff = (row-prev).values
        delta.append(diff)
        prev = row
    return pd.DataFrame(delta, columns=[' x', ' y', ' time'])

# @ usage -> creates delta csv and saves it in the destination
# @ params filenames -> relative or abs paths of the csv files
# @ destination -> where to save the delta csvs
# @ return nan
# createDeltaDataset([f for f in glob.glob('../dataset/syntTO/*csv')], '../delta_dataset/syntTO/')
def createDeltaDataset(filenames,  destination):

    for file in filenames:
        csv = pd.read_csv(file, sep='\t')[[ ' x', ' y', ' time']]
        csv = getDeltaCSV(csv)
        saveCSV(csv, destination + os.path.basename(file))

# # %%
# train_non_s = tf.data.Dataset.from_tensor_slices(getDataset('../npz/delta_csv/train.npz'.replace('scaled_', ''), 'naive'))
# test_non_s = tf.data.Dataset.from_tensor_slices(getDataset('../npz/delta_csv/test.npz'.replace('scaled_', ''), 'naive'))
# val_non_s = tf.data.Dataset.from_tensor_slices(getDataset('../npz/delta_csv/val.npz'.replace('scaled_', ''), 'naive'))
# %%
# # support functions for extracting min_max from each column of csv's
# def get_xyt_min_max(data):
#   data = data.reshape(3, 400)
#   min_x = min(data[0])
#   max_x = max(data[0])

#   min_y = min(data[1])
#   max_y = max(data[1])

#   min_t = min(data[2])
#   max_t = max(data[2])

#   return [
#     [ min_x, max_x],
#     [ min_y, max_y],
#     [ min_t, max_t]
#   ]

# def get_min_max_list(dataset):
#   real_min_max = []
#   naive_min_max = []
#   for naive, real in dataset.as_numpy_iterator():
#     naive_xyt_min_max = get_xyt_min_max(naive)
#     real_xyt_min_max = get_xyt_min_max(real)

#     real_min_max.append(real_xyt_min_max)
#     naive_min_max.append(naive_xyt_min_max)

#   return real_min_max, naive_min_max

# train_real_min_max, train_naive_min_max = get_min_max_list(train_non_s)
# test_real_min_max, test_naive_min_max = get_min_max_list(test_non_s)

# train_non_s, test_non_s, val_non_s = [], [], []
# if args.delta == 1:
#   train_non_s = tf.data.Dataset.from_tensor_slices(getDataset(args.train_dir.replace('scaled_', ''), args.x))
#   test_non_s = tf.data.Dataset.from_tensor_slices(getDataset(args.test_dir.replace('scaled', ''), args.x))
#   val_non_s = tf.data.Dataset.from_tensor_slices(getDataset(args.val_dir.replace('scaled', ''), args.x))

#   train_real_min_max, train_naive_min_max = [], []
#   test_real_min_max, test_naive_min_max = [], []

#   for naive, real in train_non_s.as_numpy_iterator():
