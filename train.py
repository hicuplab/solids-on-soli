#!/usr/bin/env python3
# coding: utf-8

'''
Train a CNN+RNN model to classify user-independent Soli-based microgestures.

External dependencies, to be installed e.g. via pip:
- tensorflow v1.13
- numpy v1.16
- Pillow v6.0

Author: Luis A. Leiva <luis.leiva@aalto.fi>
'''

import sys
import os
import random
import math
import argparse

# Define CLI options.
parser = argparse.ArgumentParser(description='Train gesture classifier on Soli data.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--dataset_dir', required=True, help='path to recorded soli data directory')
#parser.add_argument('--dataset_files', nargs='+', help='path to individual soli files. DEBUG ONLY')
parser.add_argument('--model_type', choices=['basic', 'advanced'], default='basic', help='model architecture')
parser.add_argument('--batch_size', type=int, default=32, help='train batch size')
parser.add_argument('--labels_file', default=None, help='path to labels file, one label per line')
parser.add_argument('--sampling_rate', type=int, default=0, help='sampling rate, in Hz')
parser.add_argument('--max_sequence_len', type=int, help='max length for all sequences, in timesteps')
parser.add_argument('--test_split_size', type=float, default=0.3, help='test set size, relative to the train set')
parser.add_argument('--validation_split_size', type=float, default=0.3, help='validation set size, relative to the train set')
parser.add_argument('--lr', type=float, default=0.0005, help='learning rate of the optimizer')
parser.add_argument('--epochs', type=int, default=100, help='learning rate of the optimizer')
parser.add_argument('--patience', type=int, default=10, help='early stoppping epochs')
parser.add_argument('--augmentation', action='store_true', help='perform data augmentation')

args = parser.parse_args()
print(args, file=sys.stderr)

# Display only TF errors, if any.
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import numpy as np
import tensorflow as tf
from utils import IMAGE_SHAPE, create_model, prepare_data_dir, read_class_labels, load_sequence_files, sequence_generator, label_histogram, encode_labels
from sklearn.model_selection import train_test_split

# Boost GPU usage.
conf = tf.compat.v1.ConfigProto()
conf.gpu_options.allow_growth = True
tf.compat.v1.Session(config=conf)

# Make results reproducible.
my_seed = 123456
random.seed(my_seed)
#tf.random.set_seed(my_seed) # TF >= 2 .0
np.random.seed(my_seed)

# Use current timestamp to keep individual runs of each experiment.
from datetime import datetime
NOW = datetime.now().strftime("%Y%m%d-%H%M%S")

if not os.path.isdir(args.dataset_dir):
    print('Source directory does not exist!')
    parser.print_help()
    exit()

# FIXME: Externalize common params so that train & test scripts can read the same info.
DATASET_AVG = os.path.normpath(args.dataset_dir) + '_averaged'
prepare_data_dir(args.dataset_dir, DATASET_AVG)

CLASS_LABELS = read_class_labels(DATASET_AVG, args.labels_file)
NUM_CLASSES = len(CLASS_LABELS)

sequences, orig_labels = load_sequence_files(DATASET_AVG, CLASS_LABELS)

# Convert labels to one-hot encodings, for which we need to convert labels (strings) to integers.
# Ensure that we assign the encodings in an ordered manner, to re-use them in `test.py`.
labels, mapping = encode_labels(orig_labels, CLASS_LABELS)
# Create stratified data partitions, to ensure balanced splits.
X_train, X_test, y_train, y_test = train_test_split(sequences, labels, test_size=args.test_split_size, random_state=my_seed, stratify=labels)
# Don't use the testing partition for validation. Instead, create a smaller partition.
X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=args.validation_split_size, random_state=my_seed, stratify=y_train)

# Compute hash to verify that partitions are created the same way both in train.py and test.py
print('Test partition hash:', hash(str(X_test)))

# -- BEGIN count ---
label_hist = label_histogram(labels, mapping)

print('-'*80)
print('All labels :', label_hist)
print('Train      :', label_histogram(y_train))
print('Validation :', label_histogram(y_valid))
print('Test       :', label_histogram(y_test))
print('-'*80)
#print ('KLEN augmentation    : ', args.augmentation)
print('Will classify {} classes.'.format(NUM_CLASSES))
actual_num_classes = len(label_hist.keys())
if actual_num_classes != NUM_CLASSES:
    NUM_CLASSES = actual_num_classes
    print('Actually will classify {} classes.'.format(NUM_CLASSES))
# -- END count ---

# Ensure that *all* sequences have the same length, in all generators.
seq_frame_len = max(len(seq) for seq in sequences)

if args.max_sequence_len:
    seq_frame_len = args.max_sequence_len

# Setup the data generators. For making predcitions DON'T shuffle the data.
train_generator = sequence_generator(X_train, y_train, batch_size=args.batch_size, maxlen=seq_frame_len, rate=args.sampling_rate, augmentation=args.augmentation)
valid_generator = sequence_generator(X_valid, y_valid, batch_size=args.batch_size, maxlen=seq_frame_len, rate=args.sampling_rate, augmentation=args.augmentation)
test_generator = sequence_generator(X_test, y_test, batch_size=args.batch_size, maxlen=seq_frame_len, rate=args.sampling_rate, augmentation=args.augmentation, shuffle=False)

# The input shape of our CNN+LSTM model is `(sequence_len, img_width, img_height, img_channels)`.
# Images are grayscaled, so the number of channels is 1.
input_shape = (args.batch_size, seq_frame_len, ) + IMAGE_SHAPE
print('Input shape is {}'.format(input_shape))

model = create_model(input_shape, labels.shape, type=args.model_type)
model.summary()

# Configure the model for training.
model.compile(optimizer=tf.keras.optimizers.Adam(lr=args.lr), loss='categorical_crossentropy', metrics=['acc'])

# Create a unique model ID for later reuse.
model_id = '{}_c{}b{}e{}p{}l{}s{}_{}'.format(args.model_type, NUM_CLASSES, args.batch_size, args.epochs, args.patience, seq_frame_len, args.sampling_rate, NOW)

# Setup some useful callbacks.
visualizer = tf.keras.callbacks.TensorBoard(log_dir='./tfevents_{}'.format(model_id))
earlystops = tf.keras.callbacks.EarlyStopping(patience=args.patience, restore_best_weights=True, verbose=0) # , monitor='val_acc', mode='max')

# Train the model.
model.fit_generator(
    train_generator,
    workers=0, # TODO: Make generator thread-safe and set `use_multiprocessing=True`.
    epochs=args.epochs,
    steps_per_epoch=len(X_train) // args.batch_size or 1,
    validation_data=valid_generator,
    validation_steps=len(X_valid) // args.batch_size or 1,
    callbacks=[visualizer, earlystops],
)

# Finally save the model. Maybe don't save it in the same dir as the preprocessed dataset.
model_file = os.path.join(DATASET_AVG, '{}.h5'.format(model_id))
model.save(model_file)
print('Model saved as {}'.format(model_file))

# Evaluate model on the train partition.
# Should be the same value as in the last epoch.
loss, accuracy = model.evaluate_generator(
    train_generator,
    workers=0, # TODO: Make generator thread-safe and set `use_multiprocessing=True`.
    steps=len(X_train) // args.batch_size or 1,
)
print('Train accuracy: {:.2f}%'.format(100*accuracy))

# Evaluate model on the validation partition.
# Should be the same value as in the last epoch.
loss, accuracy = model.evaluate_generator(
    valid_generator,
    workers=0, # TODO: Make generator thread-safe and set `use_multiprocessing=True`.
    steps=len(X_valid) // args.batch_size or 1,
)
print('Validation accuracy: {:.2f}%'.format(100*accuracy))

# Evaluate model on the test partition.
# Don't shuffle the data to get the same results as in test.py
loss, accuracy = model.evaluate_generator(
    test_generator,
    workers=0, # TODO: Make generator thread-safe and set `use_multiprocessing=True`.
    steps=len(X_test) // args.batch_size or 1,
)
print('Test accuracy: {:.2f}%'.format(100*accuracy))

